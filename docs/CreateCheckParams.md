
# CreateCheckParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**postingDate** | [**Date**](Date.md) |  |  [optional]
**individualName** | **String** |  |  [optional]
**accountType** | [**AccountType**](AccountType.md) |  | 
**amount** | [**BigDecimal**](BigDecimal.md) |  | 
**routingNumber** | **String** |  | 
**accountNumber** | **String** |  | 
**checkNumber** | **String** |  | 
**micr** | **String** |  |  [optional]
**entryClass** | [**EntryClass**](EntryClass.md) |  | 
**clientTag** | **String** |  |  [optional]
**addenda** | **List&lt;String&gt;** |  |  [optional]
**frontImageEncoded** | **String** |  |  [optional]
**rearImageEncoded** | **String** |  |  [optional]
**attachments** | [**List&lt;CreateAttachmentParams&gt;**](CreateAttachmentParams.md) |  |  [optional]



