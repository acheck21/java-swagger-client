# MerchantsApi

All URIs are relative to *https://gateway.acheck21.com/GlobalGateway*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createMerchant**](MerchantsApi.md#createMerchant) | **PUT** /api/v1/merchants | Create a new merchant account.
[**getMerchant**](MerchantsApi.md#getMerchant) | **GET** /api/v1/merchants/{merchantId} | Get details of a specific merchant
[**getMerchants**](MerchantsApi.md#getMerchants) | **GET** /api/v1/merchants | Get a list of all merchant accounts the user has access to
[**updateMerchant**](MerchantsApi.md#updateMerchant) | **POST** /api/v1/merchants/{merchantId} | Updates properties of a merchant account.


<a name="createMerchant"></a>
# **createMerchant**
> Merchant createMerchant(merchant)

Create a new merchant account.

### Example
```java
// Import classes:
//import com.acheck21.checks.ApiClient;
//import com.acheck21.checks.ApiException;
//import com.acheck21.checks.Configuration;
//import com.acheck21.checks.auth.*;
//import com.acheck21.checks.api.MerchantsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

MerchantsApi apiInstance = new MerchantsApi();
Merchant merchant = new Merchant(); // Merchant | 
try {
    Merchant result = apiInstance.createMerchant(merchant);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MerchantsApi#createMerchant");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant** | [**Merchant**](Merchant.md)|  | [optional]

### Return type

[**Merchant**](Merchant.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getMerchant"></a>
# **getMerchant**
> Merchant getMerchant(merchantId)

Get details of a specific merchant

### Example
```java
// Import classes:
//import com.acheck21.checks.ApiClient;
//import com.acheck21.checks.ApiException;
//import com.acheck21.checks.Configuration;
//import com.acheck21.checks.auth.*;
//import com.acheck21.checks.api.MerchantsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

MerchantsApi apiInstance = new MerchantsApi();
String merchantId = "merchantId_example"; // String | 
try {
    Merchant result = apiInstance.getMerchant(merchantId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MerchantsApi#getMerchant");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchantId** | **String**|  |

### Return type

[**Merchant**](Merchant.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getMerchants"></a>
# **getMerchants**
> List&lt;ClientName&gt; getMerchants()

Get a list of all merchant accounts the user has access to

### Example
```java
// Import classes:
//import com.acheck21.checks.ApiClient;
//import com.acheck21.checks.ApiException;
//import com.acheck21.checks.Configuration;
//import com.acheck21.checks.auth.*;
//import com.acheck21.checks.api.MerchantsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

MerchantsApi apiInstance = new MerchantsApi();
try {
    List<ClientName> result = apiInstance.getMerchants();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MerchantsApi#getMerchants");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;ClientName&gt;**](ClientName.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateMerchant"></a>
# **updateMerchant**
> Merchant updateMerchant(merchantId, client)

Updates properties of a merchant account.

### Example
```java
// Import classes:
//import com.acheck21.checks.ApiClient;
//import com.acheck21.checks.ApiException;
//import com.acheck21.checks.Configuration;
//import com.acheck21.checks.auth.*;
//import com.acheck21.checks.api.MerchantsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

MerchantsApi apiInstance = new MerchantsApi();
String merchantId = "merchantId_example"; // String | 
Merchant client = new Merchant(); // Merchant | 
try {
    Merchant result = apiInstance.updateMerchant(merchantId, client);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MerchantsApi#updateMerchant");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchantId** | **String**|  |
 **client** | [**Merchant**](Merchant.md)|  | [optional]

### Return type

[**Merchant**](Merchant.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

