# ChecksApi

All URIs are relative to *https://gateway.acheck21.com/GlobalGateway*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createCheck**](ChecksApi.md#createCheck) | **PUT** /api/v1/checks/{clientId} | Create a new Check transaction.
[**findReturns**](ChecksApi.md#findReturns) | **GET** /api/v1/checks/{clientId}/returns | Retrieve returns for given date(s) or yesterday if no dates are given
[**getAttachments**](ChecksApi.md#getAttachments) | **GET** /api/v1/checks/{clientId}/{documentId}/attachments | Retrieve Attachments for a specific Check. Usually there is at most one settlement per check.
[**getCheck**](ChecksApi.md#getCheck) | **GET** /api/v1/checks/{clientId}/{documentId} | Retrieve a specific Check
[**getChecks**](ChecksApi.md#getChecks) | **GET** /api/v1/checks/{clientId} | Search for all Checks that belong to a merchant account.
[**getReturns**](ChecksApi.md#getReturns) | **GET** /api/v1/checks/{clientId}/{documentId}/returns | Retrieve returns for a specific Check.
[**getSettlements**](ChecksApi.md#getSettlements) | **GET** /api/v1/checks/{clientId}/{documentId}/settlements | Retrieve Settlements for a specific Check. Usually there is at most one settlement per check.
[**validateCheck**](ChecksApi.md#validateCheck) | **POST** /api/v1/checks/{clientId}/validate | Validates a check. This usually involves verifying the account exists and that there are no unpaid items (a.k.a. bounced checks) associated with the account. This is configured in the Global Gateway&#39;s client manager.
[**voidCheck**](ChecksApi.md#voidCheck) | **DELETE** /api/v1/checks/{clientId}/{documentId} | Delete a specific Check transaction.


<a name="createCheck"></a>
# **createCheck**
> Check createCheck(clientId, values)

Create a new Check transaction.

### Example
```java
// Import classes:
//import com.acheck21.checks.ApiClient;
//import com.acheck21.checks.ApiException;
//import com.acheck21.checks.Configuration;
//import com.acheck21.checks.auth.*;
//import com.acheck21.checks.api.ChecksApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

ChecksApi apiInstance = new ChecksApi();
String clientId = "clientId_example"; // String | 
CreateCheckParams values = new CreateCheckParams(); // CreateCheckParams | 
try {
    Check result = apiInstance.createCheck(clientId, values);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ChecksApi#createCheck");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**|  |
 **values** | [**CreateCheckParams**](CreateCheckParams.md)|  | [optional]

### Return type

[**Check**](Check.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="findReturns"></a>
# **findReturns**
> ReturnsQueryResult findReturns(clientId, startDate, endDate, offset, limit)

Retrieve returns for given date(s) or yesterday if no dates are given

### Example
```java
// Import classes:
//import com.acheck21.checks.ApiClient;
//import com.acheck21.checks.ApiException;
//import com.acheck21.checks.Configuration;
//import com.acheck21.checks.auth.*;
//import com.acheck21.checks.api.ChecksApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

ChecksApi apiInstance = new ChecksApi();
String clientId = "clientId_example"; // String | 
Date startDate = new Date(); // Date | 
Date endDate = new Date(); // Date | 
Integer offset = 56; // Integer | 
Integer limit = 56; // Integer | 
try {
    ReturnsQueryResult result = apiInstance.findReturns(clientId, startDate, endDate, offset, limit);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ChecksApi#findReturns");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**|  |
 **startDate** | **Date**|  | [optional]
 **endDate** | **Date**|  | [optional]
 **offset** | **Integer**|  | [optional]
 **limit** | **Integer**|  | [optional]

### Return type

[**ReturnsQueryResult**](ReturnsQueryResult.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getAttachments"></a>
# **getAttachments**
> List&lt;Attachment&gt; getAttachments(clientId, documentId)

Retrieve Attachments for a specific Check. Usually there is at most one settlement per check.

### Example
```java
// Import classes:
//import com.acheck21.checks.ApiClient;
//import com.acheck21.checks.ApiException;
//import com.acheck21.checks.Configuration;
//import com.acheck21.checks.auth.*;
//import com.acheck21.checks.api.ChecksApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

ChecksApi apiInstance = new ChecksApi();
String clientId = "clientId_example"; // String | 
Long documentId = 789L; // Long | 
try {
    List<Attachment> result = apiInstance.getAttachments(clientId, documentId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ChecksApi#getAttachments");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**|  |
 **documentId** | **Long**|  |

### Return type

[**List&lt;Attachment&gt;**](Attachment.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getCheck"></a>
# **getCheck**
> Check getCheck(clientId, documentId)

Retrieve a specific Check

### Example
```java
// Import classes:
//import com.acheck21.checks.ApiClient;
//import com.acheck21.checks.ApiException;
//import com.acheck21.checks.Configuration;
//import com.acheck21.checks.auth.*;
//import com.acheck21.checks.api.ChecksApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

ChecksApi apiInstance = new ChecksApi();
String clientId = "clientId_example"; // String | 
Long documentId = 789L; // Long | 
try {
    Check result = apiInstance.getCheck(clientId, documentId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ChecksApi#getCheck");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**|  |
 **documentId** | **Long**|  |

### Return type

[**Check**](Check.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getChecks"></a>
# **getChecks**
> CheckQueryResult getChecks(clientId, startDate, endDate, currentStates, offset, limit)

Search for all Checks that belong to a merchant account.

### Example
```java
// Import classes:
//import com.acheck21.checks.ApiClient;
//import com.acheck21.checks.ApiException;
//import com.acheck21.checks.Configuration;
//import com.acheck21.checks.auth.*;
//import com.acheck21.checks.api.ChecksApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

ChecksApi apiInstance = new ChecksApi();
String clientId = "clientId_example"; // String | 
Date startDate = new Date(); // Date | 
Date endDate = new Date(); // Date | 
List<String> currentStates = Arrays.asList("currentStates_example"); // List<String> | 
Integer offset = 56; // Integer | 
Integer limit = 56; // Integer | 
try {
    CheckQueryResult result = apiInstance.getChecks(clientId, startDate, endDate, currentStates, offset, limit);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ChecksApi#getChecks");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**|  |
 **startDate** | **Date**|  | [optional]
 **endDate** | **Date**|  | [optional]
 **currentStates** | [**List&lt;String&gt;**](String.md)|  | [optional] [enum: Pending, Rejected, Deleted, Suspect, Settling, Settled, Returned]
 **offset** | **Integer**|  | [optional]
 **limit** | **Integer**|  | [optional]

### Return type

[**CheckQueryResult**](CheckQueryResult.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getReturns"></a>
# **getReturns**
> List&lt;ModelReturn&gt; getReturns(clientId, documentId)

Retrieve returns for a specific Check.

### Example
```java
// Import classes:
//import com.acheck21.checks.ApiClient;
//import com.acheck21.checks.ApiException;
//import com.acheck21.checks.Configuration;
//import com.acheck21.checks.auth.*;
//import com.acheck21.checks.api.ChecksApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

ChecksApi apiInstance = new ChecksApi();
String clientId = "clientId_example"; // String | 
Long documentId = 789L; // Long | 
try {
    List<ModelReturn> result = apiInstance.getReturns(clientId, documentId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ChecksApi#getReturns");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**|  |
 **documentId** | **Long**|  |

### Return type

[**List&lt;ModelReturn&gt;**](ModelReturn.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getSettlements"></a>
# **getSettlements**
> List&lt;Settlement&gt; getSettlements(clientId, documentId)

Retrieve Settlements for a specific Check. Usually there is at most one settlement per check.

### Example
```java
// Import classes:
//import com.acheck21.checks.ApiClient;
//import com.acheck21.checks.ApiException;
//import com.acheck21.checks.Configuration;
//import com.acheck21.checks.auth.*;
//import com.acheck21.checks.api.ChecksApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

ChecksApi apiInstance = new ChecksApi();
String clientId = "clientId_example"; // String | 
Long documentId = 789L; // Long | 
try {
    List<Settlement> result = apiInstance.getSettlements(clientId, documentId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ChecksApi#getSettlements");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**|  |
 **documentId** | **Long**|  |

### Return type

[**List&lt;Settlement&gt;**](Settlement.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="validateCheck"></a>
# **validateCheck**
> ValidationResult validateCheck(clientId, values)

Validates a check. This usually involves verifying the account exists and that there are no unpaid items (a.k.a. bounced checks) associated with the account. This is configured in the Global Gateway&#39;s client manager.

### Example
```java
// Import classes:
//import com.acheck21.checks.ApiClient;
//import com.acheck21.checks.ApiException;
//import com.acheck21.checks.Configuration;
//import com.acheck21.checks.auth.*;
//import com.acheck21.checks.api.ChecksApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

ChecksApi apiInstance = new ChecksApi();
String clientId = "clientId_example"; // String | 
ValidateCheckParams values = new ValidateCheckParams(); // ValidateCheckParams | 
try {
    ValidationResult result = apiInstance.validateCheck(clientId, values);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ChecksApi#validateCheck");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**|  |
 **values** | [**ValidateCheckParams**](ValidateCheckParams.md)|  | [optional]

### Return type

[**ValidationResult**](ValidationResult.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="voidCheck"></a>
# **voidCheck**
> voidCheck(clientId, documentId)

Delete a specific Check transaction.

### Example
```java
// Import classes:
//import com.acheck21.checks.ApiClient;
//import com.acheck21.checks.ApiException;
//import com.acheck21.checks.Configuration;
//import com.acheck21.checks.auth.*;
//import com.acheck21.checks.api.ChecksApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

ChecksApi apiInstance = new ChecksApi();
String clientId = "clientId_example"; // String | 
Long documentId = 789L; // Long | 
try {
    apiInstance.voidCheck(clientId, documentId);
} catch (ApiException e) {
    System.err.println("Exception when calling ChecksApi#voidCheck");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**|  |
 **documentId** | **Long**|  |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

