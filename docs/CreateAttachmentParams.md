
# CreateAttachmentParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**type** | [**AttachmentType**](AttachmentType.md) |  | 
**imageEncoded** | **String** |  | 



