
# Merchant

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**merchantId** | **String** |  |  [optional]
**clientName** | **String** |  |  [optional]
**achCompanyName** | **String** |  |  [optional]
**contactPerson** | **String** |  |  [optional]
**contactPhone** | **String** |  |  [optional]
**contactEmail** | **String** |  |  [optional]
**receivingRoutingNumber** | **String** |  |  [optional]
**receivingDDANumber** | **String** |  |  [optional]
**billingRoutingNumber** | **String** |  |  [optional]
**billingDDANumber** | **String** |  |  [optional]
**discretionaryData** | **String** |  |  [optional]
**securityKey** | **String** |  |  [optional]
**isChecking** | **Boolean** |  |  [optional]
**isActive** | **Boolean** |  |  [optional]
**canUpload** | **Boolean** |  |  [optional]
**isDemoAccount** | **Boolean** |  |  [optional]
**reportsTo** | **String** |  |  [optional]
**terminalCity** | **String** |  |  [optional]
**terminalState** | **String** |  |  [optional]
**routingModel** | [**RoutingModel**](RoutingModel.md) |  |  [optional]
**defaultECC** | [**EntryClass**](EntryClass.md) |  |  [optional]
**autoBatchApproval** | **Boolean** |  |  [optional]
**noOffsetFunding** | **Boolean** |  |  [optional]
**allowDuplicateItems** | **Boolean** |  |  [optional]
**highRisk** | **Boolean** |  |  [optional]
**allowEditAmount** | **Boolean** |  |  [optional]
**validEntryClassCodes** | [**List&lt;ValidEntryClassCodesEnum&gt;**](#List&lt;ValidEntryClassCodesEnum&gt;) |  |  [optional]
**odfiAccount** | **String** |  |  [optional]
**useClearingAccount** | **Boolean** |  |  [optional]
**disburseReleaseDays** | **Integer** |  |  [optional]
**holdFunds** | **Boolean** |  |  [optional]
**aggregateDisbursements** | **Boolean** |  |  [optional]
**aggregateDisbursementsByBank** | **Boolean** |  |  [optional]
**autoRejectHandling** | **Boolean** |  |  [optional]
**rejectHoldingDays** | **Integer** |  |  [optional]
**recoveryAccountId** | **String** |  |  [optional]
**reserveBalance** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**reserveRequirement** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**reserveFundlingLevel** | **Integer** |  |  [optional]
**isSuspectEnabled** | **Boolean** |  |  [optional]
**transactionSoftLimit** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**transactionLimit** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**dailySoftLimit** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**dailyLimit** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**dailyFileLimit** | **Integer** |  |  [optional]
**monthlySoftLimit** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**monthlyLimit** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**monthlyFileLimit** | **Integer** |  |  [optional]
**rvdRuleSet** | **String** |  |  [optional]
**useSPS** | **Boolean** |  |  [optional]
**requirePreAuth** | **Boolean** |  |  [optional]


<a name="List<ValidEntryClassCodesEnum>"></a>
## Enum: List&lt;ValidEntryClassCodesEnum&gt;
Name | Value
---- | -----
TEL | &quot;TEL&quot;
PPD | &quot;PPD&quot;
ARC | &quot;ARC&quot;
RCK | &quot;RCK&quot;
CCD | &quot;CCD&quot;
WEB | &quot;WEB&quot;
BOC | &quot;BOC&quot;
C21 | &quot;C21&quot;
POP | &quot;POP&quot;



