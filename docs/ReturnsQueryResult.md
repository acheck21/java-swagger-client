
# ReturnsQueryResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recordsTotal** | **Integer** |  | 
**offset** | **Integer** |  | 
**limit** | **Integer** |  | 
**first** | [**ResourceLink**](ResourceLink.md) |  |  [optional]
**prev** | [**ResourceLink**](ResourceLink.md) |  |  [optional]
**next** | [**ResourceLink**](ResourceLink.md) |  |  [optional]
**last** | [**ResourceLink**](ResourceLink.md) |  |  [optional]
**returns** | [**List&lt;ModelReturn&gt;**](ModelReturn.md) |  |  [optional]



