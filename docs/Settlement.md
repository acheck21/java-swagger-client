
# Settlement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**documentId** | **Long** |  | 
**traceNumber** | **String** |  |  [optional]
**amount** | [**BigDecimal**](BigDecimal.md) |  | 
**receivedOn** | [**Date**](Date.md) |  | 
**settledOn** | [**Date**](Date.md) |  |  [optional]



