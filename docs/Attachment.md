
# Attachment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**type** | [**AttachmentType**](AttachmentType.md) |  | 
**imageUrl** | **String** |  |  [optional]



