
# Check

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**href** | **String** |  |  [optional]
**documentId** | **Long** |  | 
**clientId** | **String** |  | 
**clientTag** | **String** |  |  [optional]
**individualName** | **String** |  |  [optional]
**companyName** | **String** |  |  [optional]
**accountType** | [**AccountType**](AccountType.md) |  | 
**amount** | [**BigDecimal**](BigDecimal.md) |  | 
**routingNumber** | **String** |  | 
**accountNumber** | **String** |  | 
**checkNumber** | **String** |  | 
**entryClass** | [**EntryClass**](EntryClass.md) |  | 
**entryDescription** | **String** |  |  [optional]
**receivedOn** | [**Date**](Date.md) |  | 
**editedOn** | [**Date**](Date.md) |  |  [optional]
**editedBy** | **String** |  |  [optional]
**deletedOn** | [**Date**](Date.md) |  |  [optional]
**deletedBy** | **String** |  |  [optional]
**addenda** | **List&lt;String&gt;** |  |  [optional]
**effectiveDate** | [**Date**](Date.md) |  |  [optional]
**postingDate** | [**Date**](Date.md) |  |  [optional]
**error** | **String** |  |  [optional]
**currentState** | [**TransactionState**](TransactionState.md) |  | 
**frontImageUrl** | **String** |  |  [optional]
**rearImageUrl** | **String** |  |  [optional]
**settlements** | [**ResourceLink**](ResourceLink.md) |  |  [optional]
**returns** | [**ResourceLink**](ResourceLink.md) |  |  [optional]
**attachments** | [**ResourceLink**](ResourceLink.md) |  |  [optional]



