
# ValidationResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**ValidationResponseType**](ValidationResponseType.md) |  | 
**detailLines** | **List&lt;String&gt;** |  |  [optional]



