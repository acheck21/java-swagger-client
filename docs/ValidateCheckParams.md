
# ValidateCheckParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**individualName** | **String** |  |  [optional]
**amount** | [**BigDecimal**](BigDecimal.md) |  | 
**routingNumber** | **String** |  | 
**accountNumber** | **String** |  | 
**checkNumber** | **String** |  | 
**micr** | **String** |  |  [optional]
**dlNumber** | **String** |  |  [optional]
**phoneNumber** | **String** |  |  [optional]



