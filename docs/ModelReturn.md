
# ModelReturn

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**documentId** | **Long** |  | 
**traceNumber** | **String** |  |  [optional]
**amount** | [**BigDecimal**](BigDecimal.md) |  | 
**receivedOn** | [**Date**](Date.md) |  | 
**returnedOn** | [**Date**](Date.md) |  |  [optional]
**returnCode** | **String** |  |  [optional]
**returnMessage** | **String** |  |  [optional]



