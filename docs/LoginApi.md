# LoginApi

All URIs are relative to *https://gateway.acheck21.com/GlobalGateway*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getCurrentUser**](LoginApi.md#getCurrentUser) | **GET** /api/v1/login | Returns currently logged in valid UserName
[**login**](LoginApi.md#login) | **POST** /api/v1/login | Login and generate cookie for future authorization.


<a name="getCurrentUser"></a>
# **getCurrentUser**
> String getCurrentUser()

Returns currently logged in valid UserName

### Example
```java
// Import classes:
//import com.acheck21.checks.ApiClient;
//import com.acheck21.checks.ApiException;
//import com.acheck21.checks.Configuration;
//import com.acheck21.checks.auth.*;
//import com.acheck21.checks.api.LoginApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

LoginApi apiInstance = new LoginApi();
try {
    String result = apiInstance.getCurrentUser();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LoginApi#getCurrentUser");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**String**

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="login"></a>
# **login**
> login(login)

Login and generate cookie for future authorization.

### Example
```java
// Import classes:
//import com.acheck21.checks.ApiClient;
//import com.acheck21.checks.ApiException;
//import com.acheck21.checks.Configuration;
//import com.acheck21.checks.auth.*;
//import com.acheck21.checks.api.LoginApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

LoginApi apiInstance = new LoginApi();
Credentials login = new Credentials(); // Credentials | 
try {
    apiInstance.login(login);
} catch (ApiException e) {
    System.err.println("Exception when calling LoginApi#login");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **login** | [**Credentials**](Credentials.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

