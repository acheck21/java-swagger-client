
# TransactionState

## Enum


* `PENDING` (value: `"Pending"`)

* `REJECTED` (value: `"Rejected"`)

* `DELETED` (value: `"Deleted"`)

* `SUSPECT` (value: `"Suspect"`)

* `SETTLING` (value: `"Settling"`)

* `SETTLED` (value: `"Settled"`)

* `RETURNED` (value: `"Returned"`)



