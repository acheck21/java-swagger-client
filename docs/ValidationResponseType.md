
# ValidationResponseType

## Enum


* `AUTHORIZED` (value: `"Authorized"`)

* `DECLINED` (value: `"Declined"`)

* `WARNING` (value: `"Warning"`)

* `ERROR` (value: `"Error"`)



