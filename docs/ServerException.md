
# ServerException

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**exceptionMessage** | **String** |  |  [optional]
**exceptionType** | **String** |  |  [optional]
**stackTrace** | **String** |  |  [optional]



