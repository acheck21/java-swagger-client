# checks

## Requirements

Building the API client library requires [Maven](https://maven.apache.org/) to be installed.

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn deploy
```

Refer to the [official documentation](https://maven.apache.org/plugins/maven-deploy-plugin/usage.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
    <groupId>com.acheck21.java</groupId>
    <artifactId>checks</artifactId>
    <version>1.0-SNAPSHOT</version>
    <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "com.acheck21.java:checks:1.0-SNAPSHOT"
```

### Others

At first generate the JAR by executing:

    mvn package

Then manually install the following JARs:

* target/checks-1.0-SNAPSHOT.jar
* target/lib/*.jar

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import com.acheck21.checks.*;
import com.acheck21.checks.auth.*;
import com.acheck21.checks.model.*;
import com.acheck21.checks.api.ChecksApi;

import java.io.File;
import java.util.*;

public class ChecksApiExample {

    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        
        // Configure HTTP basic authorization: basicAuth
        HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
        basicAuth.setUsername("YOUR USERNAME");
        basicAuth.setPassword("YOUR PASSWORD");

        ChecksApi apiInstance = new ChecksApi();
        String clientId = "clientId_example"; // String | 
        CreateCheckParams values = new CreateCheckParams(); // CreateCheckParams | 
        try {
            Check result = apiInstance.createCheck(clientId, values);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling ChecksApi#createCheck");
            e.printStackTrace();
        }
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *https://gateway.acheck21.com/GlobalGateway*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*ChecksApi* | [**createCheck**](docs/ChecksApi.md#createCheck) | **PUT** /api/v1/checks/{clientId} | Create a new Check transaction.
*ChecksApi* | [**getAttachments**](docs/ChecksApi.md#getAttachments) | **GET** /api/v1/checks/{clientId}/{documentId}/attachments | Retrieve Attachments for a specific Check. Usually there is at most one settlement per check.
*ChecksApi* | [**getCheck**](docs/ChecksApi.md#getCheck) | **GET** /api/v1/checks/{clientId}/{documentId} | Retrieve a specific Check
*ChecksApi* | [**getChecks**](docs/ChecksApi.md#getChecks) | **GET** /api/v1/checks/{clientId} | Search for all Checks that belong to a merchant account.
*ChecksApi* | [**getReturns**](docs/ChecksApi.md#getReturns) | **GET** /api/v1/checks/{clientId}/{documentId}/returns | Retrieve returns for a specific Check.
*ChecksApi* | [**getSettlements**](docs/ChecksApi.md#getSettlements) | **GET** /api/v1/checks/{clientId}/{documentId}/settlements | Retrieve Settlements for a specific Check. Usually there is at most one settlement per check.
*ChecksApi* | [**validateCheck**](docs/ChecksApi.md#validateCheck) | **POST** /api/v1/checks/{clientId}/validate | Validates a check. This usually involves verifying the account exists and that there are no unpaid items (a.k.a. bounced checks) associated with the account. This is configured in the Global Gateway&#39;s client manager.
*ChecksApi* | [**voidCheck**](docs/ChecksApi.md#voidCheck) | **DELETE** /api/v1/checks/{clientId}/{documentId} | Delete a specific Check transaction.
*LoginApi* | [**getCurrentUser**](docs/LoginApi.md#getCurrentUser) | **GET** /api/v1/login | Returns currently logged in valid UserName
*LoginApi* | [**login**](docs/LoginApi.md#login) | **POST** /api/v1/login | Login and generate cookie for future authorization.


## Documentation for Models

 - [AccountType](docs/AccountType.md)
 - [Attachment](docs/Attachment.md)
 - [AttachmentType](docs/AttachmentType.md)
 - [Check](docs/Check.md)
 - [CheckQueryResult](docs/CheckQueryResult.md)
 - [CreateAttachmentParams](docs/CreateAttachmentParams.md)
 - [CreateCheckParams](docs/CreateCheckParams.md)
 - [Credentials](docs/Credentials.md)
 - [EntryClass](docs/EntryClass.md)
 - [ModelReturn](docs/ModelReturn.md)
 - [ResourceLink](docs/ResourceLink.md)
 - [ServerException](docs/ServerException.md)
 - [Settlement](docs/Settlement.md)
 - [TransactionState](docs/TransactionState.md)
 - [ValidateCheckParams](docs/ValidateCheckParams.md)
 - [ValidationResponseType](docs/ValidationResponseType.md)
 - [ValidationResult](docs/ValidationResult.md)


## Documentation for Authorization

Authentication schemes defined for the API:
### basicAuth

- **Type**: HTTP basic authentication


## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issue.

## Author



